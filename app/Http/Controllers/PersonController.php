<?php

namespace App\Http\Controllers;

use Response;
use App\Models\Persons;
use Illuminate\Http\Request;

class PersonController extends Controller
{
      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $persons = Persons::latest()->paginate(5);
    
        return view('persons.index',compact('persons'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }
     
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('persons.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'address' => 'required',
        ]);
    
        Persons::create($request->all());
     
        return redirect()->route('persons.index')
                        ->with('success','Person created successfully.');
    }
     
    /**
     * Display the specified resource.
     *
     * @param  \App\Persons  $persons
     * @return \Illuminate\Http\Response
     */
    public function show(Persons $person)
    {
        return view('persons.show',compact('person'));
    } 
     
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Persons  $persons
     * @return \Illuminate\Http\Response
     */
    public function edit(Persons $person)
    {
        return view('persons.edit',compact('person'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Persons  $persons
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Persons $person)
    {
        $request->validate([
            'name' => 'required',
            'address' => 'required',
        ]);
    
        $person->update($request->all());
    
        return redirect()->route('persons.index')
                        ->with('success','Person updated successfully');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Person  $persons
     * @return \Illuminate\Http\Response
     */
    public function destroy(Persons $person)
    {
        $person->delete();
    
        return redirect()->route('persons.index')
                        ->with('success','Person deleted successfully');
    }

    /**
     * update access 
     *
     * @return \Illuminate\Http\Response
     */
    public function updateAccess(Request $request)
    {
       $person = Persons::find($request->id);
       
       $person->access = $request->value;
       $person->save();
       
       return Response::json(array(
        'success' => 'Person updated successfully'
          )); 
    }
}
