@extends('persons.layout')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>code</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('persons.create') }}"> Create New Person</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Address</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($persons as $person)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $person->name }}</td>
            <td>{{ $person->address }}</td>
            <td>
                <form action="{{ route('persons.destroy',$person->id) }}" method="POST">
                    <a class="btn btn-info" onclick="open_win(<?php echo $person->id; ?>)" >Show</a>
   
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
  
    {!! $persons->links() !!}
      
@endsection

<script>
    function open_win(id) {
        var base_url = {!! json_encode(url('/')) !!};
        window.open(base_url+"/persons/"+id)
        window.open(base_url+"/persons/"+id+"/edit")
    }
</script>