@extends('persons.layout')
  
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Show Person</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('persons.index') }}"> Back</a>
            </div>
        </div>
    </div>
   
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Name:</strong>
                {{ $person->name }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Address:</strong>
                {{ $person->address }}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Access:</strong>
                <input type="checkbox" value="{{$person->access}}" name="access" data-id="{{$person->id}}" id="dataAccess" {{$person->access == 1 ? 'checked' : ''}}>
            </div>
        </div>
        <span id="response"></span>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Terms and conditions:</strong>
                <input type="checkbox" id="read" name="terms and condition" />            </div>
        </div>
    </div>
@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
    $(document).ready(function() {

        $('#read').on('change', function(){
            this.value = this.checked ? 1 : 0;
        // alert(this.value);
        }).change();

        $('#dataAccess').on('click', function(){
        this.value = this.checked ? 1 : 0;

        var value = $('#dataAccess').val();
        var base_url = {!! json_encode(url('/')) !!};
        
            var id = $('#dataAccess').attr('data-id');
                
                $.ajax( {
                    type: 'POST',
                    url: base_url+'/updateAccess/'+id+'/'+this.value,
                    data: {"_token": "{{ csrf_token() }}"},
                    success: function(data) {
                        console.log(data);
                        var a = '<span style="color:red;">'+data.success+'</span>'
                        $('#response').html(a);
                    }
                } );
        }).change();
    });
</script>